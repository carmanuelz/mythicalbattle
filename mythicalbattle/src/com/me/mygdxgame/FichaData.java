package com.me.mygdxgame;

public class FichaData {
	public int IDFicha;
	public String nombre;
	public String descripcion;
	public int HP;
	public int IEP;
	public int DP;
	public int AP;
	public int MRP;
	public int AHPRP;
	public int idHabilidad1;
	public int idHabilidad2;
	public int idHabilidad3;
	public int idHabilidad4;
	public int size = 1;
}
